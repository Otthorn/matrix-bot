import re
from matrix_client.checks import check_user_id

def format_arguments(args):
    """return a link and the trailing arguments from a string containing
    a formated body (html)"""
    link_re = re.search('<a.*</a>', args)

    if link_re != None:
        link = link_re.group(0)
    else:
        link = ""
    
    trainling_args = " ".join(args.split()[3:])
    return link, trailing_args

def get_id_from_link(link):
    """return the user_id of a user from from an html link"""
    for char in link:
        result = re.search('@.*(.org|.fr|.re|.net|.io)', link)
        return result.group(0)

def get_link_from_id(user_id, name):
    """recreated an html link from the user_id and the name of a user"""
    if check_user_id(user_id) == None:
        link = '<a href="https://"matrix.to/#/{}">{}</a>'.format(user_id, name)
        return link
