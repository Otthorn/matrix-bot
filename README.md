# med-maid
## Description
Un bot matrix pour les channels de la med.

## Requirement
basé sur [python-matrix-bot](https://github.com/shawnanastasio/python-matrix-bot-api), qui lui même requiert [matrix-python-sdk](https://github.com/matrix-org/matrix-python-sdk).

Il faut également un compte matrix, une clé API pour Giphy et pour OpenWeatherMap.

## Lancer le bot
```python med-maid.py```



