import configparser

from matrix_bot_api.matrix_bot_api import MatrixBotAPI
from matrix_bot_api.mcommand_handler import MCommandHandler

def menu_callback(room, event):
        args = event['content']['body'].split()
        query = " ".join(args[1:])

        if query in dico_menu:
            url = dico_menu[query]
            room.send_image(url, query)
        else:
            room.send_text("Désolé aucun résultat pour {}".format(query))

dico_menu = {
"Pres Grille" : "mxc://matrix.org/rbWXslcjKZQrhtlhMQqOskNG",
"Hoki Yaki" : "mxc://matrix.org/MKvrlaNCwluOPrAWzWNXsEav"}

def main():
    # Load configuration
    config = configparser.ConfigParser()
    config.read("config.ini")
    username = config.get("Food", "username")
    password = config.get("Food", "password")
    server = config.get("Food", "server")

    # Start bot
    bot = MatrixBotAPI(username, password, server)

    # Create the handlers 
    menu_handler = MCommandHandler("menu",menu_callback)
    bot.add_handler(menu_handler)

    # Start polling
    bot.start_polling()
    print("food_bot started")

    while True:
        input()


if __name__ == "__main__":
    main()
