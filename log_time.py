from time import strftime

def log_time():
    return strftime("%d/%m/%Y %H:%M:%S")
