base_quote_file = "citations.txt"

def write_to_db(author, quote, quote_file=base_quote_file):
    f = open(quote_file, "a")
    f.write(author + ";;" + quote + "\n")
    f.close()

def load_db(quote_file=base_quote_file):
    
    print("Loading quote database...")

    f = open(quote_file, "r")
    L = f.readlines()
    f.close()

    quote_dico = {}

    for k in range(len(L)):
        L[k] = L[k].strip("\n")
        L[k] = L[k].split(";;")
        author = L[k][0]
        quote = L[k][1]

        if author not in quote_dico.keys():
            quote_dico[author] = []

        quote_dico[author].append(quote)

    print("Quote databse loaded")        

    return quote_dico
