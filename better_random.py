from random import shuffle

dico = {}


def cycle(L):
    """cycle one iteration of a list"""
    return L[1:] + [L[0]]


def get_random_id(query, maximum_id):
    if maximum_id == 0:
        return 0
    if query not in dico:
        list_id = list(range(maximum_id))
        shuffle(list_id)
        dico[query] = list_id
    else:
        dico[query] = cycle(dico[query])

    return dico[query][0]    
