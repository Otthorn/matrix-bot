from matrix_bot_api.matrix_bot_api import MatrixBotAPI
from matrix_bot_api.mregex_handler import MRegexHandler
from matrix_bot_api.mcommand_handler import MCommandHandler

import random
from matrix_user_id import format_arguments, get_id_from_link, get_link_from_id
import configparser 
import citations


# define the callbacks
def hi_callback(room, event):
    # Somebody said hi, let's say Hi back
    room.send_text("Bonjour, " + event['sender'])


def echo_callback(room, event):
    args = event['content']['body'].split()
    args.pop(0)

    # Echo what they said back
    room.send_text(' '.join(args))


def dieroll_callback(room, event):
    args = event['content']['body'].split()

    die = args[0]
    die_max = die[2:]

    # ensure the die is a positive integer
    if not die_max.isdigit():
        room.send_text('{} is not a positive number!'.format(die_max))
        return

    # and ensure it's a reasonable size, to prevent bot abuse
    die_max = int(die_max)
    if die_max <= 1 or die_max >= 1000:
        room.send_text('dice must be between 1 and 1000!')
        return

    # finally, send the result back
    result = random.randrange(1,die_max+1)
    room.send_text(str(result))

def help_callback(room, event):
    args = event['content']['body'].split()
    
    if len(args) == 1:
        room.send_html("<p><strong>Commandes disponibles</strong> :</p>\n\
<ul>\n\
<li><em>!help</em> <strong>X</strong>  permet de trouver de l'aide sur <strong>X</strong>;\
</li>\n\
<li><em>!d<strong>N</strong></em> donne le résultat d'un dé à <strong>N</strong> faces;</li>\n\
<li><em>!echo</em> <strong>X</strong> renvoie un message contenant <strong>X</strong>;</li>\n</ul>\n") 
    
    elif args[1] in ("d", "dice", "die", "dés", "dé", "dN", "!dN"):
        room.send_html("<em>!d<strong>N</strong></em> est une commande pour \
lancer un dé à <strong>N</strong> face(s). <strong>N</strong> entre 1 et 1000.\
<br /><em>Exemple</em> : <code>!d20</code> pour lancer un dé à 20 faces.")
        return
    
    elif args[1] in ("echo", "!echo"):
        room.send_html("<em>!echo</em> <strong>X</strong> renvoie un message \
contenant <strong>X</strong>.<br /><em>Exemple</em> : <code>!echo Med</code> \
renverra le message <code>Med</code>.")
        return
    
    else:
        return


def quote_callback(room, event):
    args = event['content']['body'].split()
    author = args[1]

    # author risque de changer si les gens changent de nom -> il vaut mieux mettre l'ID matrix    

    quote = " ".join(args[2:])

    if author not in quote_dico.keys():
        quote_dico[author] = []
    
    quote_dico[author].append(quote)
    citations.write_to_db(author, quote)

    room.send_text("Ajouter à la liste des citations de {} - (total : {})".format(author, len(quote_dico[author])))
 
def list_quote_callback(room, event):
    #args = event['content']['body'].split()
    #author = " ".join(args[1:])
    
    if 'formatted_body' in event['content'].keys():
        args = event['content']['formatted_body']
    else:
        args = event['content']['body']

    user_id = get_id_from_link(args)
    author_obj = bot.client.get_user(user_id)
    author = author_obj.get_friendly_name()

    if len(args) == 1:
        print("LISTE QUOTE agrs = 1")
        message = "Liste des personnes ayant des citations :\n<ul>\n"
        for p in quote_dico.keys():
            message += "<li>{} ({})</li>\n".format(p, len(quote_dico[p]))
        message += "</ul>\n"
        room.send_html(message)
        return

    elif author not in quote_dico.keys():
        room.send_text("Désolé mais {} est introuvable dans la base de donnée".format(author))
        return

    else:
        for quote in quote_dico[author]:
            room.send_html("<blockquote>\n{} — {}\n</blockquote>\n".format(quote, get_link_from_id(user_id)))


def main():
    # Load the configuration
    config = configparser.ConfigParser()
    config.read("config.ini")
    username = config.get("Matrix", "username")
    password = config.get("Matrix", "password")
    server = config.get("Matrix", "server")

    # Start bot
    bot = MatrixBotAPI(username, password, server)

    # Add a regex handler waiting for the word Hi
    hi_handler = MRegexHandler("Bonjour|Bonsoir|bonjour|bonsoir|Salut|salut", hi_callback)
    bot.add_handler(hi_handler)

    # Add a regex handler waiting for the echo command
    echo_handler = MCommandHandler("echo", echo_callback)
    bot.add_handler(echo_handler)

    # Add a regex handler waiting for the die roll command
    dieroll_handler = MCommandHandler("d", dieroll_callback)
    bot.add_handler(dieroll_handler)

    help_handler = MCommandHandler("help", help_callback)
    bot.add_handler(help_handler)

    quote_handler = MCommandHandler("citation", quote_callback)
    bot.add_handler(quote_handler) 

    list_quote_handler = MCommandHandler("liste_citations", list_quote_callback)
    bot.add_handler(list_quote_handler)

    # Start polling
    bot.start_polling()
    print("med_maid started")

    # Infinitely read stdin to stall main thread while the bot runs in other threads
    while True:
        input()

if __name__ == "__main__":
    main()
