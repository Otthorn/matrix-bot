# -*- coding: utf-8 -*-

from time import sleep
import better_random
import json
from urllib.request import urlopen
from urllib.parse import quote_plus
import configparser
from log_time import log_time

from matrix_bot_api.matrix_bot_api import MatrixBotAPI
from matrix_bot_api.mcommand_handler import MCommandHandler


def gif_search(query, query_type, api_key, limit):
    
    query = quote_plus(query) # make the query browser compliant

    json_obj = urlopen("http://api.giphy.com/v1/"+query_type+"/search?q=" + query + "&api_key=" + api_key + "&limit=" + limit).read() 
    data = json.loads(json_obj.decode('utf-8'))

    maximum_id = len(data['data']) - 1

    if maximum_id == -1:
        return query
   
    #random_id = random.randint(0, maximum_id)
    random_id = better_random.get_random_id(query, maximum_id)    

    print("Search id: {}/{}".format(random_id, maximum_id))

    id_url = data['data'][random_id]['id']
    url_gif = "https://media2.giphy.com/media/"+id_url+"/200.gif"
    
    mime = "image/gif" # mime type of the image
    
    data_bytes = urlopen(url_gif).read()

    return data_bytes, mime


class MyMatrixBotAPI(MatrixBotAPI):

    def __init__(self, username, password, server, api_key, limit, rooms=None):
        super().__init__(username, password, server, rooms=None)

        self.api_key = api_key
        self.limit = limit


    def generic_callback(self, query_type, room, event):

        args = event['content']['body'].split()
        query = " ".join(args[1:])

        sender = event['sender']

        if "_discord_" in sender:
            print("message envoyé par qqun de discord !")
            return

        print("[{}] {} called with query={} in room={}".format(log_time(), query_type, query, room.display_name))

        result = gif_search(query, "gifs", self.api_key, self.limit)
        if type(result) == str:
            room.send_text("Désolé, aucun résultat pour {}".format(query))
            return
        else:
            data, mime = result
        
        mxc_url = self.client.upload(data, mime) 

        room.send_image(mxc_url, query, mimetype=mime)
        #print(mxc_url)


    def gif_callback(self, room, event):
        self.generic_callback("gifs", room, event)
        return    

    def sticker_callback(self, room, event):
        self.generic_callback("stickers", room, event)
        return
    
    def reaction_callback(self, room, event):
        self.generic_callback("stickers/packs/reactions", room, event)
        return

def main():
    # Load the configuration
    config = configparser.ConfigParser()
    config.read("config.ini")
    username = config.get("Gif", "username")
    password = config.get("Gif", "password")
    server = config.get("Gif", "server")
    api_key = config.get("Gif", "api_key")
    limit = config.get("Gif", "limit")   
 

    try:
        # Start bot
        bot = MyMatrixBotAPI(username, password, server, api_key, limit)

        # Created handler 
        gif_handler = MCommandHandler("gif", bot.gif_callback)
        bot.add_handler(gif_handler)

        sticker_handler = MCommandHandler("sticker", bot.sticker_callback)
        bot.add_handler(sticker_handler)
     
        reaction_handler = MCommandHandler("reaction", bot.reaction_callback)
        bot.add_handler(reaction_handler)

        # Start polling
        bot.start_polling()
        print("gif_bot started")
        
        #while True:
        #    input()
    except:
        print("Error")
        print("...")

if __name__ == "__main__":
    while True:    
        print("STARTING MAIN")
        main()
        sleep(3600)
